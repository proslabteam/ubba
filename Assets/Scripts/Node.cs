﻿using System.Collections.Generic;

public class Node{

	public string Id { get; set; }
	public string Name { get; set; }
	public string XPosition { get; set; }
	public string YPosition { get; set; }
	public TypeOfNode Type { get; set; }
	public List<string> Outgoings = new List<string>();
	public Dictionary<string, bool> Incomings = new Dictionary<string, bool>();
}
