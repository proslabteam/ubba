﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using System.Collections.Generic;

public class Token
{
    public int? Id { get; set; }
    public string Name { get; set; }
    public string XPosition { get; set; }
    public string YPosition { get; set; }
    public string CurrentNodeId { get; set; }
    public string NextNodeId { get; set; }
    public string PreviousNodeId { get; set; }
    public List<Token> DuplicatedTokens = new List<Token>();
    public NavMeshAgent Agent { get; set; }
    public TokenMovement TokenMovement { get; set; }
    public GameObject Obj { get; set; }
    public int? OriginalTokenId { get; set; } 
    public void AddDuplicate(Token _token)
    {
        DuplicatedTokens.Add(_token);
    }
    public TypeOfToken TypeOfToken { get; set; }
    public bool ButtonHasBeenCreated { get; set; }
    public bool IsButtonActive { get; set; }

    public bool IsPaused { get; set; }

}

