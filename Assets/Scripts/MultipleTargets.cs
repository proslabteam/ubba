﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MultipleTargets : MonoBehaviour
{
    public List<Transform> targets;
    public Vector3 offset;
    public float smoothTime = 0.5f;
    private Vector3 velocity;
    public float minZoom = 70f;
    public float maxZoom = 30f;
    public float zoomLimiter = 40f;
    private Camera cam;
    public float zoomSpeed = 5f;
    public bool moved = false;
    FileLoader fileLoader;

    void Start()
    {
        cam = GetComponent<Camera>();
        GameObject _canvas = GameObject.Find("Canvas");
        fileLoader = _canvas.GetComponent<FileLoader>();
    }

    void Update()
    {

        if (fileLoader.fileHasBeenLoaded)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            //Quando premo Esc ritorno allo zoom iniziale
            if (Input.GetKey(KeyCode.Escape))
            {
                fileLoader.TurnOffAllCameras();
                fileLoader.ActivateMainCamera();
                Move();
            }

            if (Physics.Raycast(ray, out hit, 100))
                Debug.DrawLine(ray.origin, hit.point, Color.green);

            // Allows zooming in and out via the mouse wheel
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                if (transform.position.y < GetGreatestDistance())
                {
                    transform.Translate(0, 1, 0, Space.World);
                    transform.position = new Vector3(transform.position.x, transform.position.y + 10, transform.position.z);
                }
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                if (transform.position.y > 20)
                {
                    transform.Translate(0, -1, 0, Space.World);
                    transform.position = new Vector3(hit.point.x, transform.position.y - 10, hit.point.z);
                }
                //transform.position = new Vector3(hit.point.x, Mathf.Clamp(transform.position.y, 1, 2), hit.point.z);
            }
        }

        /*if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit point;
            Physics.Raycast(ray, out point, 25);
           
            Vector3 ScrollDirection = ray.GetPoint(5);
            Debug.Log("scroll direction " + ScrollDirection);
            float step = zoomSpeed * Time.deltaTime;
            Debug.Log("step " + step);
            transform.position = Vector3.MoveTowards(transform.position, ScrollDirection, Input.GetAxis("Mouse ScrollWheel") * step);
        }*/
        /*if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            cam.fieldOfView--;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            cam.fieldOfView++;
        }*/
    }

    void LateUpdate()
    {
        if (targets.Count == 0)
        {
            return;
        }

        if (!moved)
        {
            Move();
            moved = true;
        }
    }

    void Move()
    {
        Vector3 centerPoint = GetCenterPoint();
        var greatestDistance = GetGreatestDistance();

        if (greatestDistance > 100)
        {
            offset = new Vector3(0, greatestDistance / 2, 0);
        }
        else
        {
            offset = new Vector3(0, 46, 0);
        }

        Vector3 newPosition = centerPoint + offset;
        transform.position = newPosition;
        //transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
    }

    Vector3 GetCenterPoint()
    {
        if (targets.Count == 1)
        {
            return targets[0].position;
        }

        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.center;
    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.size.x;
    }

    public void AddTarget(Transform _transform)
    {
        targets.Add(_transform);
    }


}
