﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausedToken 
{
	public string NodeId { get; set; }
	public Token Token { get; set; }

}
