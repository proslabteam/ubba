﻿

public enum TypeOfNode {
	StartEvent,
	Task,
	EndEvent,
	ExclusiveGateway,
	ParallelGateway,
	SendTask,
	ReceiveTask,
	EventBasedGateway,
	IntermediateCatchEvent,
	IntermediateThrowEvent
}
