﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

	public bool paused;
	public Text pausedButtonText;

	// Use this for initialization
	void Start () {
		paused = false;
	}

	public void CambiaStato() {
		
		if(pausedButtonText.text == "Pause"){
			pausedButtonText.text = "Play";
		} else {
			pausedButtonText.text = "Pause";
		}

		paused = !paused;

		
	}
	
	// Update is called once per frame
	void Update () {
		if(paused) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
		
	}
}
