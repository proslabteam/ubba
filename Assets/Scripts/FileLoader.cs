﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFileBrowser;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.AI;
using System;
using SimpleJSON;
using Newtonsoft;


public class FileLoader : MonoBehaviour
{

    string path;
    public Text testo;
    public List<SequenceFlow> sequenceFlows = new List<SequenceFlow>();
    //public List<Node> nodes = new List<Node> ();
    public Dictionary<string, string> successors = new Dictionary<string, string>();
    public Dictionary<string, string> previous = new Dictionary<string, string>();
    public List<Participant> participants = new List<Participant>();
    public List<Process> process = new List<Process>();
    public Dictionary<string, TypeOfNode> typeOfNodes = new Dictionary<string, TypeOfNode>();
    //List<TextPosition> textPosition = new List<TextPosition>();


    public string[] tests;

    public GameObject StartEvent;
    public GameObject EndEvent;
    public GameObject Task;
    public GameObject SequenceFlow;

    public Button simulationButton;

    public GameObject cam;
    public MultipleTargets multipleTargets;
    public GameObject token;
    public List<Token> tokens = new List<Token>();
    public GameObject buttonPrefab;
    public GameObject cloneTokenButtonPrefab;
    public Transform buttonContainer;
    public List<TokenButton> tokenButtons = new List<TokenButton>();
    //Per controllare se il file xml è stato caricato oppure no
    public bool fileHasBeenLoaded = false;
    public int numberOfProcess = 0;
    //La lista dei token in pausa
    public List<PausedToken> pausedTokens = new List<PausedToken>();
    //Used to import 3d models 
    private ObjectImporter objImporter;

    int numberOfNodeToLoad = 0;
    int numberOfCurrentProcess = 0;
    int totalNumberOfProcesses = 0;
    int numberOfTokenToLoad = 0;
    public GameObject UploadMenu;
    public GameObject SelectObjPanel;
    public Button NextButton;
    public Button LoadButton;
    //Used for Model loaded from PC
    public Text ElementName;
    //Used for Models already loaded
    public Text ElementName2;
    public Font myFont;
    //Contains all the models already loaded
    private List<GameObject> models = new List<GameObject>();
    //The index of the selected model
    private int selectionIndex = 0;
    public GameObject SelectObjCamera;
    //Assigned for the active camera and used for node's names
    private GameObject textPositionCamera;
    public Material lineMaterial;
    private int tokenSelectionIndex = 0;
    public List<string> tokenList = new List<string>();
    public List<string> tokenIdList = new List<string>();
    public GameObject UploadOrDefaultTokenMenu;
    public GameObject SelectTokenCamera;
    public GameObject SelectTokenPanel;
    public Text TokenName;
    private List<GameObject> tokenModels = new List<GameObject>();

    GameObject activeCamera;
    public GameObject Text3d;
    List<GameObject> nameOfTasks = new List<GameObject>();
    Dictionary<string, int> userChoices = new Dictionary<string, int>();
    private bool saveOptions = false;
    string fileName;
    public GameObject UseFileWithChoices;
    public GameObject Background;
    public GameObject Walls;

    private GameObject chosenToken;

    void Start()
    {
        multipleTargets = cam.GetComponent<MultipleTargets>();
        objImporter = gameObject.AddComponent<ObjectImporter>();

        FileBrowser.SetFilters(true, new FileBrowser.Filter("BPMN files", ".bpmn"));
        FileBrowser.SetDefaultFilter(".bpmn");
        OpenExplorer();

        //da modificare
        textPositionCamera = SelectObjCamera;
    }

    void Update()
    {

        if (Input.GetMouseButton(0) && activeCamera != null && fileHasBeenLoaded)
        {
            activeCamera.transform.Rotate(new Vector3(0.0f, Input.GetAxis("Mouse X"), 0.0f));
        }

        foreach (Token t in tokens)
        {
            bool exists = false;
            foreach (TokenButton tokenButton in tokenButtons)
            {
                if (t.ButtonHasBeenCreated)
                {
                    exists = true;
                    if (t.Obj == null)
                    {
                        Destroy(tokenButton.TokenButtonObject);
                    }
                }
            }

            if (!exists && t.TypeOfToken != TypeOfToken.Message && t.TypeOfToken != TypeOfToken.Clone)
            {
                tokenButtons.Add(new TokenButton()
                {
                    Id = t.Id,
                    Name = t.Name,
                    HasBeenCreated = false,
                    IsClone = false
                });
                t.ButtonHasBeenCreated = true;
            }
            else if (!exists && t.TypeOfToken == TypeOfToken.Clone)
            {
                tokenButtons.Add(new TokenButton()
                {
                    Id = t.Id,
                    Name = t.Name,
                    HasBeenCreated = false,
                    IsClone = true
                });
                t.ButtonHasBeenCreated = true;
            }
        }

        foreach (TokenButton t in tokenButtons)
        {
            if (!t.HasBeenCreated && t.IsClone == false)
            {
                GameObject go = Instantiate(buttonPrefab) as GameObject;
                t.TokenButtonObject = go;
                go.GetComponentInChildren<Text>().text = t.Name;
                go.transform.SetParent(buttonContainer);

                go.GetComponent<Button>().onClick.AddListener(() => OnButtonClick(t.Id));

                t.HasBeenCreated = true;
            }
            else if (!t.HasBeenCreated && t.IsClone == true)
            {
                GameObject go = Instantiate(cloneTokenButtonPrefab) as GameObject;
                t.TokenButtonObject = go;
                go.GetComponentInChildren<Text>().text = t.Name;
                go.transform.SetParent(buttonContainer);

                go.GetComponent<Button>().onClick.AddListener(() => OnButtonClick(t.Id));

                t.HasBeenCreated = true;
            }
        }

        if (nameOfTasks.Count != 0)
        {
            if (activeCamera != null)
            {
                foreach (GameObject _text in nameOfTasks)
                {
                    _text.transform.LookAt(activeCamera.transform.position);
                    Vector3 textRotation = _text.transform.rotation.eulerAngles;

                    Vector3 newRotation = new Vector3(textRotation.x, textRotation.y + 180, textRotation.z);

                    _text.transform.rotation = Quaternion.Euler(newRotation);
                }
            }
        }

    }

    public void OnButtonClick(int? _id)
    {
        cam.gameObject.GetComponent<Camera>().enabled = false;
        TurnOffAllCameras();

        foreach (Token _token in tokens)
        {
            if (_token.Id == _id)
            {
                _token.Obj.transform.Find("Camera").GetComponent<Camera>().enabled = true;
                setTextPositionCamera(_token.Obj.transform.GetChild(0).gameObject);
            }
        }

        //Ceiling.GetComponent<Renderer>().material = Non_Transparent;
        //Walls.SetActive(true);
    }

    public void ActiveButton(int? _id)
    {
        foreach (TokenButton _tokenButton in tokenButtons)
        {
            if (_tokenButton.Id == _id && !_tokenButton.IsClone)
            {
                _tokenButton.TokenButtonObject.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void DisableButton(int? _id)
    {
        foreach (TokenButton _tokenButton in tokenButtons)
        {
            if (_tokenButton.Id == _id && !_tokenButton.IsClone)
            {
                _tokenButton.TokenButtonObject.GetComponent<Button>().interactable = false;
            }
            else if (_tokenButton.Id == _id && _tokenButton.IsClone)
            {
                Destroy(_tokenButton.TokenButtonObject);
            }
        }
    }

    public void DeleteButton(int? _id)
    {
        foreach (TokenButton _tokenButton in tokenButtons)
        {
            if (_tokenButton.Id == _id && _tokenButton.IsClone)
            {
                Destroy(_tokenButton.TokenButtonObject);
            }
        }
    }

    public void TurnOffAllCameras()
    {
        foreach (Token _token in tokens)
        {
            if (_token.Obj.transform.Find("Camera").GetComponent<Camera>().isActiveAndEnabled)
            {
                _token.Obj.transform.Find("Camera").GetComponent<Camera>().enabled = false;
            }
        }
    }

    public void ActivateMainCamera()
    {
        cam.gameObject.GetComponent<Camera>().enabled = true;
        setTextPositionCamera(cam);
        //Ceiling.GetComponent<Renderer>().material = Transparent;
        //Walls.SetActive(false);
    }

    public void OpenExplorer()
    {

        StartCoroutine(ShowLoadDialogCoroutine());

    }

    IEnumerator ShowLoadDialogCoroutine()
    {
        yield return FileBrowser.WaitForLoadDialog(false, null, "Load File", "Load");

        if (FileBrowser.Success)
        {
            path = FileBrowser.Result;
            fileName = Path.GetFileNameWithoutExtension(path);
            GetContent();
        }

    }

    void GetContent()
    {
        if (path != null)
        {
            WWW www = new WWW("file:///" + path);

            ParseXMLData(www.text);
        }
    }

    void ParseXMLData(string xmlData)
    {

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader(xmlData));

        XmlNodeList mainNode = xmlDoc.GetElementsByTagName("bpmn:definitions");

        foreach (XmlNode node in mainNode)
        {
            foreach (XmlNode node1 in node)
            {
                if (node1.Name == "bpmn:process")
                {
                    foreach (XmlNode node2 in node1)
                    {
                        if (node2.Name == "bpmn:sequenceFlow")
                        {
                            successors.Add(node2.Attributes["id"].Value, node2.Attributes["targetRef"].Value);
                            previous.Add(node2.Attributes["id"].Value, node2.Attributes["sourceRef"].Value);
                        }
                    }

                    Process p = new Process();

                    numberOfProcess++;
                    p.Id = node1.Attributes["id"].Value;
                    p.Number = numberOfProcess;

                    foreach (XmlNode node2 in node1)
                    {
                        if (node2.Name == "bpmn:task")
                        {
                            AddNode(node2, TypeOfNode.Task, p);
                        }
                        else if (node2.Name == "bpmn:startEvent")
                        {
                            AddNode(node2, TypeOfNode.StartEvent, p);
                        }
                        else if (node2.Name == "bpmn:endEvent")
                        {
                            AddNode(node2, TypeOfNode.EndEvent, p);
                        }
                        else if (node2.Name == "bpmn:exclusiveGateway")
                        {
                            AddNode(node2, TypeOfNode.ExclusiveGateway, p);
                        }
                        else if (node2.Name == "bpmn:parallelGateway")
                        {
                            AddNode(node2, TypeOfNode.ParallelGateway, p);
                        }
                        else if (node2.Name == "bpmn:sendTask")
                        {
                            AddNode(node2, TypeOfNode.SendTask, p);
                        }
                        else if (node2.Name == "bpmn:receiveTask")
                        {
                            AddNode(node2, TypeOfNode.ReceiveTask, p);
                        }
                        else if (node2.Name == "bpmn:eventBasedGateway")
                        {
                            AddNode(node2, TypeOfNode.EventBasedGateway, p);
                        }
                        else if (node2.Name == "bpmn:intermediateCatchEvent")
                        {
                            AddNode(node2, TypeOfNode.IntermediateCatchEvent, p);
                        }
                        else if (node2.Name == "bpmn:intermediateThrowEvent")
                        {
                            AddNode(node2, TypeOfNode.IntermediateThrowEvent, p);
                        }
                    }
                    process.Add(p);
                }
                else if (node1.Name == "bpmndi:BPMNDiagram")
                {
                    foreach (XmlNode node2 in node1)
                    {
                        if (node2.Name == "bpmndi:BPMNPlane")
                        {
                            foreach (XmlNode node3 in node2)
                            {
                                if (node3.Name == "bpmndi:BPMNShape")
                                {
                                    foreach (Process p in process)
                                    {
                                        foreach (Node n in p.Nodes)
                                        {
                                            if (n.Id == node3.Attributes["bpmnElement"].Value)
                                            {
                                                n.XPosition = node3.FirstChild.Attributes["x"].Value;
                                                n.YPosition = node3.FirstChild.Attributes["y"].Value;
                                            }
                                        }

                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        foreach (XmlNode node in mainNode)
        {
            foreach (XmlNode node1 in node)
            {
                if (node1.Name == "bpmn:collaboration")
                {

                    foreach (XmlNode node2 in node1)
                    {
                        if (node2.Name == "bpmn:participant")
                        {
                            participants.Add(new Participant()
                            {
                                Id = node2.Attributes["id"].Value,
                                Name = node2.Attributes["name"].Value,
                                ProcessRef = node2.Attributes["processRef"].Value
                            });
                        }
                        else if (node2.Name == "bpmn:messageFlow")
                        {
                            foreach (Process p in process)
                            {
                                foreach (Node _node in p.Nodes)
                                {
                                    if (_node.Id == node2.Attributes["sourceRef"].Value)
                                    {
                                        _node.Outgoings.Add(node2.Attributes["targetRef"].Value);
                                    }
                                    else if (_node.Id == node2.Attributes["targetRef"].Value)
                                    {
                                        _node.Incomings.Add(node2.Attributes["sourceRef"].Value, false);
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        CreateWorld();

    }


    private void AddNode(XmlNode _node, TypeOfNode _type, Process _p)
    {

        List<string> _outgoings = new List<string>();
        Dictionary<string, bool> _incomings = new Dictionary<string, bool>();

        /*if (_type == TypeOfNode.EndEvent) {

		} else {

		}*/

        foreach (XmlNode child in _node)
        {
            if (child.Name == "bpmn:outgoing")
            {
                string value = successors[child.InnerText];
                _outgoings.Add(value);
            }
            else if (child.Name == "bpmn:incoming")
            {
                string value = previous[child.InnerText];
                _incomings.Add(value, false);
            }

        }

        string id = _node.Attributes["id"].Value;

        //DEVO CONTROLLARE SE IL NODO HA UN NOME OPPURE MENO

        string name;

        if (_node.Attributes["name"] != null)
        {
            name = _node.Attributes["name"].Value;
        }
        else
        {
            name = "";
        }

        _p.AddNode(id, name, _outgoings, _type, _incomings);

        //typeOfNodes.Add (id, TypeOfNode.Task);

    }

    private void CreateWorld()
    {
        foreach (Process p in process)
        {
            totalNumberOfProcesses++;
            foreach (Node n in p.Nodes)
            {
                //InstantiateNode(n);
            }
            //simulationButton.gameObject.SetActive(true);
        }
        Background.SetActive(false);
        UploadMenu.SetActive(true);
        string path = Application.persistentDataPath + "/" + fileName + ".json";
        if(!File.Exists(path))
        {
            UseFileWithChoices.SetActive(false);
        }
            

        NextButton.interactable = false;
        ElementName.text = process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Name;
        //InstantiateNodes();

        //InitiateSimulation();
        //fileHasBeenLoaded = true;
    }

    //THIS FUNCTION IS CALLED WHEN NEXT BUTTON IS PRESSED
    public void InstantiateNodes()
    {
        if (numberOfNodeToLoad < process[numberOfCurrentProcess].Nodes.Count - 1)
        {
            numberOfNodeToLoad++;
            ElementName.text = process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Name;
            NextButton.interactable = false;
            LoadButton.interactable = true;
            //InstantiateNode(process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad]);
            Debug.Log("loading number of node " + (numberOfNodeToLoad) + " of process " + (numberOfCurrentProcess));

        }
        else
        {
            numberOfCurrentProcess++;
            if (numberOfCurrentProcess >= totalNumberOfProcesses)
            {
                InitiateSimulation();
                UploadMenu.SetActive(false);
                SelectObjPanel.SetActive(false);
                SelectObjCamera.SetActive(false);

                UploadOrDefaultTokenMenu.SetActive(true);
                SelectTokenCamera.SetActive(true);

                setTextPositionCamera(SelectTokenCamera);

                fileHasBeenLoaded = true;

                CreateSequenceFlows();
                Debug.Log("Loading tokens started");
            }
            else
            {
                Debug.Log("Going to next Process");
                numberOfNodeToLoad = 0;
                InstantiateNodes();
            }
        }
    }

    //THIS FUNCTION IS CALLED WHEN LOAD BUTTON IS PRESSED
    public void InstantiateNode()
    {
        Vector3 pos = new Vector3();
        Vector3 rot = new Vector3();
        string name;

        name = process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Name;

        pos.x = int.Parse(process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].XPosition.Split('.')[0]) / 10;
        pos.z = int.Parse(process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].YPosition.Split('.')[0]) / 10;

        StartCoroutine(ChooseNode(pos, rot, name));
    }

    //When UseAlreadyLoaded button is clicked
    public void UseAlreadyLoaded()
    {
        UploadMenu.SetActive(false);
        SelectObjPanel.SetActive(true);
        ElementName2.text = "Task to load: " + process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Name;
    }

    //When NextButton on UseAlreadyLoaded is clicked
    public void SelectAlreadyAdded()
    {
        if (numberOfNodeToLoad < process[numberOfCurrentProcess].Nodes.Count)
        {
            GameObject obj;
            //GameObject mySphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Vector3 pos = new Vector3();
            Node node = process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad];

            pos = Vector3.zero;
            //Alcuni punti non sono interi, in questo caso prendo solo la parte intera
            pos.x = int.Parse(node.XPosition.Split('.')[0]) / 10;
            pos.y = models[selectionIndex].transform.position.y;
            pos.z = int.Parse(node.YPosition.Split('.')[0]) / 10;
            Quaternion startSpawnRotation = models[selectionIndex].transform.rotation;

            obj = Instantiate(models[selectionIndex], pos, startSpawnRotation);
            obj.gameObject.name = node.Id;

            AddNameOfNode(obj, node.Name);

            multipleTargets.AddTarget(obj.transform);

            numberOfNodeToLoad++;
            if (numberOfNodeToLoad < process[numberOfCurrentProcess].Nodes.Count)
            {
                if(process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Name != "") {
                    ElementName2.text = process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Type + " to load: " + process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Name;
                } else {
                    ElementName2.text = process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Type + " to load: " + process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Id;
                }
                
            }
            if (saveOptions)
            {
                userChoices.Add(node.Id, selectionIndex);
            }

        }
        else
        {
            numberOfCurrentProcess++;
            if (numberOfCurrentProcess >= totalNumberOfProcesses)
            {
                InitiateSimulation();
                SelectObjPanel.SetActive(false);
                SelectObjCamera.SetActive(false);

                UploadOrDefaultTokenMenu.SetActive(true);
                SelectTokenCamera.SetActive(true);

                setTextPositionCamera(SelectTokenCamera);

                fileHasBeenLoaded = true;

                CreateSequenceFlows();
                Debug.Log("Loading tokens started");
            }
            else
            {
                Debug.Log("Going to next Process");
                numberOfNodeToLoad = 0;
                //ElementName2.text = "Task to load: " + process[numberOfCurrentProcess].Nodes[numberOfNodeToLoad].Name;
                SelectAlreadyAdded();
            }
        }
    }

    public void UseAlreadyLoadedToken()
    {
        UploadOrDefaultTokenMenu.SetActive(false);
        SelectTokenPanel.SetActive(true);
        TokenName.text = "Token to load: " + tokenList[numberOfTokenToLoad];
    }

    public void SelectAlreadyAddedToken()
    {
        if (numberOfTokenToLoad < tokenList.Count)
        {
            foreach (Process process in process)
            {
                foreach (Participant participant in participants)
                {
                    if (process.Id == participant.ProcessRef)
                    {
                        foreach (Node node in process.Nodes)
                        {
                            if (node.Id == tokenIdList[numberOfTokenToLoad])
                            {
                                SpawnTokenAlreadyLoaded(process.Number, node.XPosition, node.YPosition, node.Id);
                                //SpawnToken(process.Number, node.XPosition, node.YPosition, node.Id);
                            }
                        }

                    }
                }
            }

            numberOfTokenToLoad++;
        }
        else
        {
            SelectTokenPanel.SetActive(false);
            SelectTokenCamera.SetActive(false);

            cam.SetActive(true);
            setTextPositionCamera(cam);

            simulationButton.gameObject.SetActive(true);

            if (saveOptions)
            {
                SaveChoices();
            }
        }
    }

    private void SpawnTokenAlreadyLoaded(int _tokenId, string _x, string _y, string _currentNodeId)
    {
        Vector3 pos;
        GameObject tokenObj;
        NavMeshAgent _agent;
        TokenMovement _tokenMovement;

        Quaternion startSpawnRotation = models[selectionIndex].transform.rotation;
        //Alcuni punti non sono interi, in questo caso prendo solo la parte intera
        pos.x = int.Parse(_x.Split('.')[0]) / 10;
        pos.z = int.Parse(_y.Split('.')[0]) / 10;
        pos.y = token.transform.position.y;

        tokenObj = Instantiate(tokenModels[tokenSelectionIndex], pos, startSpawnRotation);
        
        chosenToken = tokenModels[tokenSelectionIndex];

        _agent = tokenObj.GetComponent<NavMeshAgent>();
        _tokenMovement = tokenObj.GetComponent<TokenMovement>();

        tokens.Add(new Token()
        {
            Id = _tokenId,
            Name = "Token" + _tokenId,
            CurrentNodeId = _currentNodeId,
            Agent = _agent,
            TokenMovement = _tokenMovement,
            Obj = tokenObj,
            TypeOfToken = TypeOfToken.Token,
            ButtonHasBeenCreated = false,
            IsButtonActive = true
        });
    }

    //THIS FUNCTION IS CALLED INSIDE InstantiateNode()
    IEnumerator ChooseNode(Vector3 _pos, Vector3 _rot, string _name)
    {
        yield return FileBrowser.WaitForLoadDialog(false, null, "Choose Token", "Load");

        if (FileBrowser.Success)
        {
            path = FileBrowser.Result;

            ImportOptions importOptions = new ImportOptions();


            importOptions.localPosition = _pos;
            importOptions.localEulerAngles = _rot;
            //FUNZIONANTE 
            //Libreria usata per importare modelli 3d runtime
            //https://github.com/gpvigano/AsImpL

            objImporter.ImportModelAsync(_name, path, null, importOptions, null, null, "Node");
            NextButton.interactable = true;
            LoadButton.interactable = false;
        }
        else
        {
            Debug.LogError("The node was not chosen correctly.");
        }
    }

    public void AddNameOfNode(GameObject _go, string _name)
    {
        /*GameObject mySphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        GameObject objName = new GameObject();
        objName.transform.SetParent(this.transform);

        Text newTxt = objName.AddComponent<Text>();
        newTxt.text = _name;
        newTxt.font = myFont;
        newTxt.alignment = TextAnchor.MiddleCenter;

        //Set the position of the sphere to the position of the _go + 2 on the y axis
        Vector3 pos = _go.transform.position;
        pos.y += 2;
        mySphere.transform.position = pos;
        mySphere.GetComponent<Collider>().enabled = false;
        mySphere.GetComponent<Renderer>().enabled = false;

        mySphere.transform.parent = _go.transform;

        textPosition.Add(new TextPosition()
        {
            TextTransform = objName.transform,
            Position = mySphere.transform.position
        });*/

        GameObject newTxt = Instantiate(Text3d) as GameObject;
        newTxt.GetComponent<TextMesh>().text = _name;

        Vector3 pos = _go.transform.position;
        pos.y += 2;
        newTxt.transform.position = pos;

        nameOfTasks.Add(newTxt);

        //newTxt.transform.LookAt(activeCamera.transform.position);

        multipleTargets.AddTarget(_go.transform);
    }

    /*public void InstantiateNode(Node n)
    {
        GameObject obj;
        Vector3 pos = new Vector3();

        pos = Vector3.zero;
        //Alcuni punti non sono interi, in questo caso prendo solo la parte intera
        pos.x = int.Parse(n.XPosition.Split('.')[0]) / 10;
        pos.z = int.Parse(n.YPosition.Split('.')[0]) / 10;
        Quaternion startSpawnRotation = GameObject.FindGameObjectWithTag("StartEventSpawn").transform.rotation;
        obj = Instantiate(StartEvent, pos, startSpawnRotation);
        obj.gameObject.name = n.Id;
        obj.GetComponentInChildren<TextMesh>().text = n.Name;

        multipleTargets.AddTarget(obj.transform);
    }*/

    public List<Process> GetProcess()
    {
        return process;
    }

    public List<Participant> GetParticipants()
    {
        return participants;
    }

    /*public List<Node> GetTasks() {
		return nodes;
	}*/

    public List<Token> GetTokens()
    {
        return tokens;
    }

    public Token GetToken(int? _id)
    {
        foreach (Token _token in tokens)
        {
            if (_token.Id == _id)
            {
                return _token;
            }
        }

        return null;
    }

    private void InitiateSimulation()
    {
        //cam.SetActive (false);
        foreach (Process process in process)
        {
            foreach (Participant participant in participants)
            {
                if (process.Id == participant.ProcessRef)
                {
                    foreach (Node node in process.Nodes)
                    {
                        if (node.Type == TypeOfNode.StartEvent)
                        {
                            int numberOfIncomings = GetNumberOfTokensIncoming(node.Id);
                            if (numberOfIncomings == 0)
                            {
                                tokenList.Add(participant.Name);
                                tokenIdList.Add(node.Id);
                                //SpawnToken(process.Number, node.XPosition, node.YPosition, node.Id);
                            }
                        }
                    }

                }
            }
        }
    }

    public int GetNumberOfTokensIncoming(string _id)
    {
        int count = 0;
        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _id)
                {
                    foreach (var item in _node.Incomings)
                    {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private void SpawnToken(int _tokenId, string _x, string _y, string _currentNodeId)
    {

        //GameObject obj;
        Vector3 pos;
        //NavMeshAgent _agent;
        //TokenMovement _tokenMovement;

        pos = Vector3.zero;

        //Alcuni punti non sono interi, in questo caso prendo solo la parte intera
        pos.x = int.Parse(_x.Split('.')[0]) / 10;
        pos.z = int.Parse(_y.Split('.')[0]) / 10;
        pos.y = token.transform.position.y;
        Vector3 rot = new Vector3(90, 90, 90);

        StartCoroutine(ChooseToken(pos, rot, _tokenId, _currentNodeId));
    }

    public void AddNewToken(GameObject go, int? _tokenId, string _currentNodeId)
    {
        NavMeshAgent _agent;
        TokenMovement _tokenMovement;

        var emptyObj = new GameObject("Token" + _tokenId);

        emptyObj.AddComponent<NavMeshAgent>();
        emptyObj.AddComponent<TokenMovement>();

        _agent = emptyObj.GetComponent<NavMeshAgent>();
        _tokenMovement = emptyObj.GetComponent<TokenMovement>();

        var newCam = new GameObject("Camera");

        newCam.AddComponent<Camera>().enabled = false;
        newCam.name = "Camera";
        Vector3 goPosition = go.transform.position;

        emptyObj.transform.position = goPosition;


        newCam.transform.position = new Vector3(goPosition.x, goPosition.y + 3, goPosition.z - 6);

        newCam.transform.parent = emptyObj.transform;

        go.transform.parent = emptyObj.transform;

        //emptyObj.transform.rotation = Quaternion.Euler(new Vector3(90, 90, 0));
        //emptyObj.transform.localRotation = Quaternion.Euler(new Vector3(90, 90, 0)); 
        //Vector3 _rot = new Vector3(90, 90, 0);

        //go.transform.localRotation = Quaternion.Euler(_rot); 

        tokens.Add(new Token()
        {
            Id = _tokenId,
            Name = "Token" + _tokenId,
            CurrentNodeId = _currentNodeId,
            Agent = _agent,
            TokenMovement = _tokenMovement,
            Obj = emptyObj,
            TypeOfToken = TypeOfToken.Token,
            ButtonHasBeenCreated = false,
            IsButtonActive = true
        });
    }

    IEnumerator ChooseToken(Vector3 _pos, Vector3 _rot, int _tokenId, string _currentNodeId)
    {
        yield return FileBrowser.WaitForLoadDialog(false, null, "Choose Token", "Load");

        if (FileBrowser.Success)
        {
            path = FileBrowser.Result;

            ImportOptions importOptions = new ImportOptions();
            importOptions.localPosition = _pos;
            importOptions.localEulerAngles = _rot;
            //FUNZIONANTE 
            //Libreria usata per importare modelli 3d runtime
            //https://github.com/gpvigano/AsImpL

            objImporter.ImportModelAsync("Token1", path, null, importOptions, _tokenId, _currentNodeId, "Token");
        }
    }

    public void AddToken(Token _token)
    {
        tokens.Add(_token);
    }


    public void SetTokenArrived(string _nodeId, string _incomingId)
    {

        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _nodeId)
                {
                    _node.Incomings[_incomingId] = true;
                }
            }
        }
    }

    public Token GetPausedToken(string _nodeId)
    {
        foreach (PausedToken _pausedToken in pausedTokens)
        {
            if (_pausedToken.NodeId == _nodeId)
            {
                return _pausedToken.Token;
            }
        }

        return null;
    }

    public string GetXPosition(string _nodeId)
    {
        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _nodeId)
                {
                    return _node.XPosition;
                }
            }
        }

        return null;
    }

    public string GetYPosition(string _nodeId)
    {
        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _nodeId)
                {
                    return _node.YPosition;
                }
            }
        }

        return null;
    }

    public void SetModels(List<GameObject> _models)
    {
        models = _models;
    }

    public void SetTokenModels(List<GameObject> _models)
    {
        tokenModels = _models;
    }

    public void setSelectionIndex(int _index)
    {
        selectionIndex = _index;
    }

    public void setTokenSelectionIndex(int _index)
    {
        tokenSelectionIndex = _index;
    }

    public void setTextPositionCamera(GameObject _activeCam)
    {
        textPositionCamera = _activeCam;
        activeCamera = _activeCam;
    }

    private void CreateSequenceFlows()
    {

        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                Vector3 myPos = new Vector3(float.Parse(_node.XPosition) / 10, 0.2f, float.Parse(_node.YPosition) / 10);
                foreach (string _outgoing in _node.Outgoings)
                {
                    var xPos = GetXPosition(_outgoing);
                    var zPos = GetYPosition(_outgoing);

                    Vector3 newPos = new Vector3(float.Parse(xPos) / 10, 0.2f, float.Parse(zPos) / 10);

                    DrawLine drawLine = new DrawLine();
                    drawLine.Draw(myPos, newPos, lineMaterial);

                }
            }
        }
    }

    public string GetPreviousNodeId(string _nodeId)
    {
        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _nodeId)
                {
                    foreach (KeyValuePair<string, bool> entry in _node.Incomings)
                    {
                        if (IsPartOfTheSameProcess(entry.Key, _p.Id))
                        {
                            return entry.Key;
                        }
                    }
                }
            }
        }

        return null;
    }

    private bool IsPartOfTheSameProcess(string _nodeId, string _processId)
    {
        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _nodeId)
                {
                    if (_p.Id == _processId)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void ChangeMyChoice(bool _value)
    {
        saveOptions = _value;
    }

    void SaveChoices()
    {
        List<Choice> choiceList = new List<Choice>();
        foreach (var element in userChoices)
        {
            Choice choice = new Choice()
            {
                keyToSave = element.Key,
                valueToSave = element.Value
            };
            choiceList.Add(choice);
        }

        var output = Newtonsoft.Json.JsonConvert.SerializeObject(choiceList);

        string path = Application.persistentDataPath + "/" + fileName + ".json";
        File.WriteAllText(path, output.ToString());

        /*var collectionWrapper = new
        {
            myRoot = choiceList
        };

        var output = Newtonsoft.Json.JsonConvert.SerializeObject(collectionWrapper);

        string path = Application.persistentDataPath + "/" + fileName + ".json";
        File.WriteAllText(path, output.ToString());
       */
    }

    public void LoadChoices()
    {
        SelectObjCamera.SetActive(false);
        string path = Application.persistentDataPath + "/" + fileName + ".json";
        string json;
        string name;
        using (StreamReader r = new StreamReader(path))
        {
            json = r.ReadToEnd();
        }

        List<Choice> items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Choice>>(json);

        foreach (var _model in models)
        {
            _model.SetActive(true);
        }

        foreach (var item in items)
        {
            //Debug.Log(item.keyToSave + " " + item.valueToSave);
            GameObject obj;
            //GameObject mySphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Vector3 pos = new Vector3();

            pos = Vector3.zero;
            //Alcuni punti non sono interi, in questo caso prendo solo la parte intera
            pos.x = int.Parse((GetXPosition(item.keyToSave)).Split('.')[0]) / 10;
            pos.y = models[item.valueToSave].transform.position.y;
            pos.z = int.Parse((GetYPosition(item.keyToSave)).Split('.')[0]) / 10;
            Quaternion startSpawnRotation = models[item.valueToSave].transform.rotation;

            obj = Instantiate(models[item.valueToSave], pos, startSpawnRotation);
            obj.gameObject.name = item.keyToSave;

            name = GetNodeName(item.keyToSave);
            AddNameOfNode(obj, name);

            multipleTargets.AddTarget(obj.transform);
        }

        UploadMenu.SetActive(false);

        InitiateSimulation();
        UploadOrDefaultTokenMenu.SetActive(true);
        SelectTokenCamera.SetActive(true);

        setTextPositionCamera(SelectTokenCamera);

        fileHasBeenLoaded = true;

        CreateSequenceFlows();

    }

    private string GetNodeName(string _id)
    {
        foreach (Process _p in process)
        {
            foreach (Node _node in _p.Nodes)
            {
                if (_node.Id == _id)
                {
                    return _node.Name;
                }
            }
        }

        return null;
    }

    public GameObject GetChosenToken()
    {
        return chosenToken;
    }

}
