# UBBA - Unity Based BPMN Animator
 
 UBBA (Unity Based BPMN Animator) is a tool that, taken in input a BPMN file allows to create a virtual world with a 3D visualization of all the elements present inside the BPMN model. More in detail, UBBA aims at reproducing the setting of a BPMN collaboration diagram and animate its execution from the point of view of the resource, by means of token flow. Indeed, 3D shapes play the roles of the BPMN elements in a virtual space. The animation provides one or more tokens crossing the diagram following the semantics of the met elements. UBBA is cross-platform and stand-alone, it has been realized in Unity 3D, a game engine for creating 2D/3D video games or other interactive contents, such as architectural visualizations or animations in real time.

The main features of UBBA are

- loading a BPMN file in standard XML format;
- associating 3D graphics to individual elements in the BPMN model;
- animating the newly loaded model in a 3D environment;
- choosing between single token to overall collaboration views during the simulation animation.

More detailed information about tool and its usage can be found here: [pros.unicam.it]

### Installation

UBBA source code can be downloaded and imported in Unity as a project (we reccomend to use version 2017.3.1f1 downloadable here: [Unity Repository]).

To run UBBA, open the executable for your operating system from the folder

```sh
/Build/
```



   [pros.unicam.it]: <http://pros.unicam.it/ubba/>
   [Unity Repository]: <https://unity3d.com/get-unity/download/archive>
   